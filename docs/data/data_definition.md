# Definición de datos y características

Para la construcción del modelo de clasificación de diagnósticos cardíacos se han seleccionado dos conjuntos de datos que se encuentran publicados en [Physionet](https://www.physionet.org/). Los conjuntos de datos contienen información de electrocardiogramas(ECG) que serán el principal insumo para el proyecto.

## Fuentes de datos brutos

### Datos de entrenamiento

Los electrocardiogramas fueron recolectados de pacientes voluntarios en la Clínica Universitaria Benjamin Franklin en Berlín, sanos y con diferentes enfermedades cardíacas, por el profesor Michael Oeff del Departamento de Cardiología del Instituto Nacional de Metrología de Alemania.

### Datos de prueba

Los electrocardioramas fueron recolectados de pacientes voluntarios por Tatiana Lugovaya, quien la utilizó en su tesis de maestría.
</br></br>
| Nombre del conjunto de datos | Ubicación original | Ubicación de destino | Herramientas de movimiento de datos |Informe
| :---| :--- | :--- | :--- | :---|
| Datos de entrenamiento | [ECG de diagnóstico PTB](https://www.physionet.org/content/ptbdb/1.0.0/) | [datos_entrenamiento](/inputs/datos_entrenamiento.zip)|[obtener_datos.py](/scripts/data_acquisition/obtener_datos.py)|[Informe datos](/docs/data/data_summary.md)
| Datos de prueba | [ID de ECG](https://physionet.org/content/ecgiddb/1.0.0/) | [datos_prueba](/inputs/datos_prueba.zip) | [obtener_datos.py](/scripts/data_acquisition/obtener_datos.py) | [Informe datos](/docs/data/data_summary.md)|


## Datos procesados

El procesamiento realizado por el autor del artículo del cual se obtuvieron los datos, se define abajo, pues cabe aclarar que los datos ya tienen la señales procesadas para análisis
</br></br>
El ECG bruto por lo general tiene mucho ruido y contiene varias distorsiones de origen. Sin embargo, se decidió apagar todos los filtros que provee el software de cardiografía, porque no era claro si eliminaba características informativas para la identificación.
</br></br>
Tras un análisis visual de ruido del ECG, se puede ver que la etapa de preprocesamiento debería realizar tres tareas importantes: corrección de base corrida, filtrado de frecuencia relativa y mejoramiento de señal.
</br></br>
Se hizo corrección de base implementando análisis wavelet unidimensional multinivel. Esto mostró reducción en el ruido de las señales ECG
</br></br>
El filtrado de frecuencia selectiva fue implementada usando un conjunto de filtros adaptativos, lo que suprimió ruido de poder de línea.
[Procesamiento de señales](https://physionet.org/files/ecgiddb/1.0.0/biometric.shtml)

## Conjuntos de características
 
 En proceso de selección
