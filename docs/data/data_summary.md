---
output:
  html_document: default
---
# Informe de datos

## Resumen general de los datos
### Datos de entrenamiento

La base de datos de ECG contiene 549 registros de 290 pacientes en un rango de edad de 17 a 87 años.
</br></br>
En la base hay información de 209 hombres y 81 mujeres, los datos se encuentran almacenados en los archivos con extensiones .dat, .hea y .xyz en el directorio __inputs/datos_entrenamiento__ del proyecto, en particular el archivo que más información contiene sobre los pacientes es el de extensión .hea, ya que en este se encuentran los datos de caracterización de cada paciente y su respectivo diagnóstico.

### Datos de prueba

La base de datos de ECG contiene 310 registros de ECG de 90 personas en un rango de edad de 13 a 75 años.
</br></br>
En la base hay información de 44 hombres y 46 mujeres, los datos se encuentran almacenados en los archivos con extensiones .atr, .hea y .dat en el directorio __inputs/datos_prueba__, en particular el archivo que contiene la información que caracteriza las personas es el de extensión .hea, en este se encuentra la edad, el sexo y la fecha en la que se tomó el electrocardiograma.

## Resumen de la calidad de los datos

La calidad de los datos es bastante buena. Sin embargo, en los datos de entrenamiento hay datos faltantes en la variable edad de 15 pacientes (1 mujer y 14 hombres). Además, no se encuentran disponible la historia clínica de 22 pacientes. 

## Variable objetivo

La variable objetivo es el diagnóstico de admisión de los pacientes con dificultades cardíacas, dentro de las varibales se encuentra nombrada como *Diagnose:Reason for admission*

## Variables individuales y clasificación

### Datos de entrenamiento
<table><thead><tr><th>Categoría<br></th><th>Variable</th></tr></thead><tbody><tr><td>General</td><td> age</td></tr><tr><td>General</td><td> sex</td></tr><tr><td>General</td><td> ECG date</td></tr><tr><td> Diagnose</td><td> Reason for admission</td></tr><tr><td> Diagnose</td><td> Acute infarction (localization)</td></tr><tr><td> Diagnose</td><td> Former infarction (localization)</td></tr><tr><td> Diagnose</td><td> Additional diagnoses</td></tr><tr><td> Diagnose</td><td> Smoker</td></tr><tr><td> Diagnose</td><td> Number of coronary vessels involved</td></tr><tr><td> Diagnose</td><td> Infarction date (acute)</td></tr><tr><td> Diagnose</td><td> Previous infarction (1) date</td></tr><tr><td> Diagnose</td><td> Previous infarction (2) date</td></tr><tr><td> Hemodynamics</td><td> Catheterization date</td></tr><tr><td> Hemodynamics</td><td> Ventriculography</td></tr><tr><td> Hemodynamics</td><td> Chest X-ray</td></tr><tr><td> Hemodynamics</td><td> Peripheral blood Pressure (syst/diast)</td></tr><tr><td> Hemodynamics</td><td> Pulmonary artery pressure (at rest)&nbsp;&nbsp;&nbsp;(syst/diast)</td></tr><tr><td> Hemodynamics</td><td> Pulmonary artery pressure (at rest) (mean)</td></tr><tr><td> Hemodynamics</td><td> Pulmonary capillary wedge pressure (at rest)</td></tr><tr><td> Hemodynamics</td><td> Cardiac output (at rest)</td></tr><tr><td> Hemodynamics</td><td> Cardiac index (at rest)</td></tr><tr><td> Hemodynamics</td><td> Stroke volume index (at rest)</td></tr><tr><td> Hemodynamics</td><td> Pulmonary artery pressure (laod)&nbsp;&nbsp;&nbsp;(syst/diast)</td></tr><tr><td> Hemodynamics</td><td> Pulmonary artery pressure (laod) (mean)</td></tr><tr><td> Hemodynamics</td><td> Pulmonary capillary wedge pressure (load)</td></tr><tr><td> Hemodynamics</td><td> Cardiac output (load)</td></tr><tr><td> Hemodynamics</td><td> Cardiac index (load)</td></tr><tr><td> Hemodynamics</td><td> Stroke volume index (load)</td></tr><tr><td> Hemodynamics</td><td> Aorta (at rest) (syst/diast)</td></tr><tr><td> Hemodynamics</td><td> Aorta (at rest) mean</td></tr><tr><td> Hemodynamics</td><td> Left ventricular enddiastolic pressure</td></tr><tr><td> Hemodynamics</td><td> Left coronary artery stenoses (RIVA)</td></tr><tr><td> Hemodynamics</td><td> Left coronary artery stenoses (RCX)</td></tr><tr><td> Hemodynamics</td><td> Right coronary artery stenoses (RCA)</td></tr><tr><td> Hemodynamics</td><td> Echocardiography</td></tr><tr><td> Therapy</td><td> Infarction date</td></tr><tr><td> Therapy</td><td> Catheterization date</td></tr><tr><td> Therapy</td><td> Admission date</td></tr><tr><td> Therapy</td><td> Medication pre admission</td></tr><tr><td> Therapy</td><td> Start lysis therapy (hh.mm)</td></tr><tr><td> Therapy</td><td> Lytic agent</td></tr><tr><td> Therapy</td><td> Dosage (lytic agent)</td></tr><tr><td> Therapy</td><td> Additional medication</td></tr><tr><td> Therapy</td><td> In hospital medication</td></tr><tr><td> Therapy</td><td> Medication after discharge</td></tr>
<tr><td>ECG</td><td> Señal registrada</td></tr></tbody></table>

### Datos prueba

<table><thead><tr><th>Categoría</th><th>Variable</th></tr></thead><tbody><tr><td>General</td><td>Age</td></tr><tr><td>General</td><td>Sex</td></tr><tr><td>General</td><td>ECG date</td></tr><tr><td>ECG</td><td>Señal registrada</td></tr></tbody></table>

## Relación entre las variables explicativas y la variable objetivo

En proceso de evaluación




