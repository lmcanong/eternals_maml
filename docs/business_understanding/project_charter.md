<img src="Fondo_PC.jpg" width="900"/>

# Carta del proyecto

## Antecedentes empresariales
### ¿Quién es el cliente?, ¿en qué ámbito empresarial se encuentra el cliente?
El cliente es un equipo de emprendedores autodenominado *“The Scient-Heart for the world”*, el cual se encuentran en fase de prototipado y necesitan realizar el MVP para presentarlo a una ronda de ángeles inversionistas en la ciudad de New York en junio de 2022.

### ¿Qué problemas empresariales se pretenden resolver?
El cliente desea obtener un modelo analítico que permita detectar y predecir en tiempo real posibles anomalías y/o patologías cardiacas.

En la actualidad siete de las 10 principales causas de muerte son enfermedades no transmisibles, según las Estadísticas Sanitarias Mundiales 2019 de la OMS. Y en el evento realizado por la OMS en Ginebra. Suiza en diciembre del año 2020, afirman que de estas enfermedades no trasmisibles las cardiopatías siguen siendo la primera causa de mortalidad. [^1]

[^1]: https://www.paho.org/es/noticias/9-12-2020-oms-revela-principales-causas-muerte-discapacidad-mundo-2000-2019

## Alcance
### ¿Qué soluciones de ciencia de datos estamos tratando de construir?
Por medio de la metodología TDSP (Team Data Science Process) se construirá un modelo descriptivo de señales cardiacas y un modelo predictivo de cardiopatías.

### ¿Qué vamos a hacer?
Para dar solución a la necesidad del cliente, el proyecto se va a dividir en dos fases:

- **FASE 1**: Exploración y caracterización de datos y construcción de un modelo analítico que permita detectar anomalías y/o patologías cardiacas por medio de procesamiento de señales en tiempo y frecuencia.

- **FASE 2**: Construcción de un modelo analítico que permitirá realizar la predicción de posibles manifestaciones de arritmias por medio del análisis de señales electroencefalografías en tiempo real.

### ¿Cómo va a ser consumido por el cliente?
El cliente recibirá un aplicativo para ser configurado y usado por medio de una página web con un usuario y contraseña previa implementación de todos los parámetros y normativas de seguridad de la información.

## Personal
### ¿Quiénes están en este proyecto?
El proyecto será desarrollado por un equipo con habilidades y formación en diferentes campos: Estadística, científicos de datos, ingenieros de datos, ingenieros especialistas en biomédica y electrónica, ingenieros especialistas en arquitectura tecnológica en nube y en desarrollo de aplicaciones web.

* **Empresa a cargo del proyecto**: OAL SOLUTIONS
  * Jefe de proyecto: Olguer Morales
  * PM: Olguer Morales
  * Científico(s) de datos: Angélica Mora, Laura Cañon
  * Director de cuentas: Laura Cañon
* **Cliente**: The Scient-Heart for the world
	* Administrador de datos: Laura Cañon
	* Contacto con la empresa: Olguer Morales
	
## Métricas
### ¿Cuáles son los objetivos cualitativos?
Reducir la mortandad en pacientes con deficiencias cardiacas por medio de la detección y predicción de fallas cardiacas por medio de la analítica de señales electroencefalográficas.

### ¿Cuáles son las métricas cuantificables?
La principal métrica de evaluación de los modelos será el número de detección de cardiopatías en pacientes con comorbilidades.

### Cuantificar qué mejora de los valores de la métrica es útil para el escenario del cliente
Se espera reducir en un periodo de 1 año la mortandad por enfermedades del corazón en un 0,1%, es decir en aproximadamente 50 personas por año.

### ¿Cuál es el valor de referencia (actual) de la métrica?
Las enfermedades cardiovasculares son la primera causa de muerte en Colombia y el mundo. De acuerdo con cifras presentadas por el DANE, de las 242.609 muertes registradas en 2019, 38.475 correspondieron a enfermedades isquémicas del corazón y 15.543 a enfermedades cerebrovasculares. [^2]

Muertes aproximadas por año 38mil (2019) y en aumento.

[^2]: https://www.bayer.com/es/co/las-enfermedades-cardiovasculares-son-la-primera-causa-de-muerte-en-colombia-y-el-mundo#

### ¿Cómo mediremos la métrica?
Sobre el número de pacientes monitoreados.

## Plan
<img src="Cronograma_hitos_PC.png" width="900"/>

## Arquitectura
### Datos
Los datos utilizados para la construcción y validación de los modelos seran tomados de las bases de Physionet. https://physionet.org/

### ¿Qué datos esperamos?
Se esperan procesar datos en texto plano, con diferentes campos y características de señales electroencefalografías.

### ¿Qué herramientas y recursos de almacenamiento/análisis de datos se utilizarán en la solución?
Los datos se cargarán a una Azure Storage Datalake V2. Desde allí se realizará un proceso de extracción, análisis de calidad de datos para posteriormente consumirlos por medio de Azure Databricks usando lenguajes como Python, PySpark y R y a su vez también se consumirán usando las herramientas de Azure ML para el modelado y la operacionalización del aplicativo web.

### ¿Cómo utilizará el cliente los resultados del modelo para tomar decisiones?
El cliente consultara los resultados por medio de una interfaz web gráfica y por medio de un tablero con métricas construido en PowerBI.

## Comunicación
### ¿Cómo nos mantendremos en contacto?
El equipo realizará seguimiento de las actividades propuestas en el plan de trabajo mediante una reunión semanal de 30 minutos usando Microsoft Teams. En la primera parte de la reunión se dará a conocer el resultado de las actividades ejecutadas en la semana anterior. En la segunda parte de la reunión se asignarán responsabilidades a los integrantes del equipo para las actividades que serán ejecutadas la siguiente semana.

### ¿Quiénes son las personas de contacto de ambas partes?
- OAL SOLUTIONS : Olguer Morales
- The Scient-Heart for the world: Laura Cañon

