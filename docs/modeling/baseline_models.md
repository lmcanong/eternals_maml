# Informe del modelo de referencia

El modelo base es el modelo que un científico de datos entrenaría y evaluaría rápidamente después de tener el primer conjunto de características (preliminares) listo para el modelado de aprendizaje automático. A través de la construcción del modelo de referencia, el científico de datos puede tener una evaluación rápida de la viabilidad de la tarea de aprendizaje automático.


## Enfoque analítico
Se entiende que el electrocardiograma como medida de señales eléctricas del corazón, genera la información temporal suficiente para determinar características inherentes a la salud del músculo que las produce; es por esto que a través de ésta medida de voltaje se puede determinar con un alto nivel de confianza el estado en el que se encuentra el músculo.

Actualmente se requiere de la intervención humana para determinar lo que varias señales eléctricas representan para la salud de un paciente, por lo que se espera crear un modelo que clasifique correctamente el estado de salud a partir de un conjunto de datos ya etiquetado por un grupo de profesionales.

Las señales eléctricas con las que se cuenta, son tomadas con un dispositivo que dependiendo de la cantidad de canales, el voltaje de entrada, la resistencia, el ancho de banda y la forma de grabado puede generar diferentes registros según sea el interés, es por esto que se cuenta con 15 diferentes señales por individuo.

Debido a la naturaleza de estos datos, el modelo usado para el análisis de esta información es la red neuronal multicapa de perceptrones que ha probado resultados favorables para series de tiempo complejas como las de señales eléctricas debido al ruido alta variabilidad que las caracterizan.

## Descripción del modelo

* Modelo MLP:
<img src="Ejemplo_15_senales.png" width="900"/>
<img src="Ejemplo_transformada.png" width="900"/>

Los hiperparámetros de entrenamientos fueron el número máximo de iteraciones igual a 300 para cada señal.


## Resultados (Rendimiento del modelo)
Se entrenó el modelo de hiperparámetros fijos con máximo de iteraciones igual a 300 con las 15 diferentes señales eléctricas del corazón en busca de la que mejor predijera el estado de salud del músculo.

A continuación los resultados de cada señal:

|                         |Precisión    |Recall  |F1-score   |Support|
|-------------------------|-------------|--------|-----------|-------|
|           <b>Señal 1</b>  |         |    |       |    |
|               accuracy  |             |        |    0.57   |    165|
|              macro avg  |     0.09    |  0.10  |    0.09   |    165|
|           weighted avg  |     0.47    |  0.57  |    0.51   |    165|
|           <b>Señal 2</b>  |         |    |       |    |
|               accuracy    |         |            |  0.58    |   165|
|              macro avg    |   0.14  |    0.12    |  0.12    |   165|
|           weighted avg    |   0.49  |    0.58    |  0.52    |   165|
|           <b>Señal 3</b>  |         |            |          |      |
|               accuracy    |         |            |  0.54    |   165|
|              macro avg    |   0.08  |    0.09    |  0.08    |   165|
|           weighted avg    |   0.43  |    0.54    |  0.48    |   165|
|           <b>Señal 4</b>  |         |            |          |      |
|               accuracy    |         |            |  0.58    |   165|
|              macro avg    |   0.09  |    0.10    |  0.09    |   165|
|           weighted avg    |   0.46  |    0.58    |  0.51    |   165|
|           <b>Señal 5</b>  |         |            |          |      |
|               accuracy    |         |            |  0.58    |   165|
|              macro avg    |   0.08  |    0.10    |  0.08    |   165|
|           weighted avg    |   0.44  |    0.58    |  0.50    |   165|
|           <b>Señal 6</b>  |         |            |          |      |
|               accuracy    |         |            |  0.63    |   165|
|              macro avg    |   0.14  |    0.13    |  0.13    |   165|
|           weighted avg    |   0.51  |    0.63    |  0.54    |   165|
|           <b>Señal 7</b>  |         |            |          |      |
|               accuracy    |         |            |  0.62    |   165|
|              macro avg    |   0.09  |    0.10    |  0.08    |   165|
|           weighted avg    |   0.46  |    0.62    |  0.51    |   165|
|           <b>Señal 8</b>  |         |            |          |      |
|               accuracy    |         |            |  0.65    |   165|
|              macro avg    |   0.17  |    0.13    |  0.12    |   165|
|           weighted avg    |   0.55  |    0.65    |  0.56    |   165|
|           <b>Señal 9</b>  |         |            |          |      |
|               accuracy    |         |            |  0.65    |   165|
|              macro avg    |   0.11  |    0.12    |  0.11    |   165|
|           weighted avg    |   0.52  |    0.65    |  0.56    |   165|
|           <b>Señal 10</b>  |         |            |          |      |
|               accuracy    |         |            |  0.62    |   165|
|              macro avg    |   0.10  |    0.11    |  0.10    |   165|
|           weighted avg    |   0.49  |    0.62    |  0.53    |   165|
|           <b>Señal 11</b>  |         |            |          |      |
|               accuracy    |         |            |  0.60    |   165|
|              macro avg    |   0.07  |    0.09    |  0.08    |   165|
|           weighted avg    |   0.42  |    0.60    |  0.49    |   165|
|           <b>Señal 12</b>  |         |            |          |      |
|               accuracy    |         |            |  0.60    |   165|
|              macro avg    |   0.06  |    0.09    |  0.08    |   165|
|           weighted avg    |   0.41  |    0.60    |  0.49    |   165|
|           <b>Señal 13</b>  |         |            |          |      |
|               accuracy    |         |            |  0.64    |   165|
|              macro avg    |   0.09  |    0.10    |  0.08    |   165|
|           weighted avg    |   0.46  |    0.64    |  0.51    |   165|
|           <b>Señal 14</b>  |         |            |          |      |
|               accuracy    |         |            |  0.57    |   165|
|              macro avg    |   0.09  |    0.09    |  0.08    |   165|
|           weighted avg    |   0.45  |    0.57    |  0.49    |   165|
|           <b>Señal 15</b>  |         |            |          |      |
|               accuracy    |         |            |  0.64    |   165|
|              macro avg    |   0.06  |    0.10    |  0.08    |   165|
|           weighted avg    |   0.41  |    0.64    |  0.50    |   165|		   		   		   		   		   		   		   


## Comprensión del modelo

Se cuenta únicamente con las 15 señales tomadas, por lo que las variables explicativas de este modelo es únicamente el voltaje generado por el corazón.

## Conclusión y debates para los próximos pasos

* La señal 8 y 9 tienen la misma eficacia a la hora de predecir el estado de salud del músculo del corazón

* Se concluye que el modelo MLP es suficiente para la buena predicción del estado de salud del corazón según las señales eléctricas que emite.

* Características asociadas a la historia clínica previa del paciente en caso de tener más información socioeconómica.

* Otras medidas que sean posible tomarsele al paciente.

