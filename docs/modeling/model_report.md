# Informe del modelo final

## Enfoque analítico

Se entiende que el electrocardiograma como medida de señales eléctricas del corazón, genera la información temporal suficiente para determinar características inherentes a la salud del músculo que las produce; es por esto que a través de ésta medida de voltaje se puede determinar con un alto nivel de confianza el estado en el que se encuentra el músculo.

Actualmente se requiere de la intervención humana para determinar lo que varias señales eléctricas representan para la salud de un paciente, por lo que se espera crear un modelo que clasifique correctamente el estado de salud a partir de un conjunto de datos ya etiquetado por un grupo de profesionales.

Las señales eléctricas con las que se cuenta, son tomadas con un dispositivo que dependiendo de la cantidad de canales, el voltaje de entrada, la resistencia, el ancho de banda y la forma de grabado puede generar diferentes registros según sea el interés, es por esto que se cuenta con 15 diferentes señales por individuo.

Debido a la naturaleza de estos datos, el modelo usado para el análisis de esta información es la red neuronal multicapa de perceptrones que ha probado resultados favorables para series de tiempo complejas como las de señales eléctricas debido al ruido alta variabilidad que las caracterizan.

## Descripción de la solución

Para el desarrollo de la primera fase se decidió almacenar los datos de manera local. La ingesta de datos se hace desde Physionet, se desarrolló un script para descargar los datos y almacenar los datos en cualquier equipo. 

Una vez se encuentran almacenados los datos localmente, se realiza la lectura de cada uno de los datos que acompañana a los electrocardiogramas.

El resultado de la primera fase es la comparación de la efectividad de un modelo de clasifición al entrenarlo con distintos tipos de señales, en la que varía el voltaje.


## Datos

* Fuente: [ECG de diagnóstico PTB](https://www.physionet.org/content/ptbdb/1.0.0/) 

* Esquema de datos:
Cada uno de los pacientes cuenta con una o más mediciones de señales de electrocardiogramas (i, ii, iii, avr, avl, avf, v1, v2, v3, v4, v5, v6, vx, vy, vz).

<img src="Ejemplo_15_senales.png" width="900"/>

* Muestreo:
No se realizó una selección de individuos, ya que para este caso particular se tienen por lo menos una de las señales completas para cada individuo.
* Selección :
Se realiza la selección de la ventana de medición de cada una de las señales de acuerdo tomando como referencia la señal de individuo más corta.
* Estadísticas:
En total se utilizaron todas las señales de los 549 electrocardiogramas registrados en la base original, tomando una ventana de 32000 ms.

## Características
 
En particular, en este caso las características corresponden a los valores medidos en la ventana de 32000 ms selecccionada de cada señal, por lo que determinar la importancia de las características no tendría relevancia en este contexto.


## Algoritmo

Los expertos en el tema de clasificación automática de electrocardiogramas sugieren realizar una limpieza de cada una de las señales a través de la transformada wavelet db4, para este caso seleccionamos realizar la transformación discreta.

<img src="Ejemplo_transformada.png" width="900"/>

Se realizó el entrenamiento de 15  modelos de perceptrón multicapa tomando los datos transformados para reducir el ruido. Además, se almacenan los reportes de cada uno de los clasificadores para determinar cuál es el mejor.

## Resultados

Al comparar los 15 modelos entrenados se evidencia que la señal 8 o v2 es la que mejor resulta al clasificar las afecciones de los pacientes.
