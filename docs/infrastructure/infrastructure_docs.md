# Infraestructura

En esta carpeta puedes añadir documentos/guías de infraestructura que
incluyen:

## Configuración y gestión de Docker/kubernetes.

## Configuración basada en el servidor (recursos mínimos del sistema, configuración de VMs, configuración del servidor web, entre otros).

```

       _,met$$$$$gg.          grupo1@instance-1
    ,g$$$$$$$$$$$$$$$P.       -----------------
  ,g$$P"     """Y$$.".        OS: Debian GNU/Linux 10 (buster) x86_64
 ,$$P'              `$$$.     Host: Google Compute Engine
',$$P       ,ggs.     `$$b:   Kernel: 4.19.0-18-cloud-amd64
`d$$'     ,$P"'   .    $$$    Uptime: 28 days, 12 hours, 15 mins
 $$P      d$'     ,    $$P    Packages: 867 (dpkg)
 $$:      $$.   -    ,d$$'    Shell: bash 5.0.3
 $$;      Y$b._   _,d$P'      Resolution: 1920x1080
 Y$$.    `.`"Y$$$$P"'         Terminal: /dev/pts/0
 `$$b      "-.__              CPU: Intel Xeon (2) @ 2.200GHz
  `Y$$                        Memory: 255MiB / 3946MiB
   `Y$$.
     `$$b.
       `Y$$b.
          `"Y$b._
              `"""

```

## Configuración del entorno (pyenv, poetry, jupyter, rstudio).

[Archivo de configuración de poetry]( pyproject.toml)

## Pipelines de ejecución (airflow, mlflow).


