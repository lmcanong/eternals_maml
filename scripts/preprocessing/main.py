import os
import csv
import wfdb
# import numpy as np
import pandas as pd

from glob import glob

ruta_gr = r'/Users/lauramel/eternals_maml/'
# =============================================================================
# Datos prueba
# =============================================================================
ruta = os.path.join(ruta_gr,r'inputs/datos_prueba')
#personas = glob(os.path.join(ruta,'ecg-id-database-1.0.0','P*','*'))
personas = glob(os.path.join(ruta,'P*','*'))
personas = list(set([os.path.splitext(a)[0] for a in personas]))

result = []
for per in personas:
    tmp = dict()
    rec_tmp = wfdb.rdrecord(per).__dict__['comments']
    tmp['Persona'] = os.path.basename(os.path.dirname(per))
    tmp['Grabacion'] = os.path.basename(per)
    para_sumar = {a.split(':')[0]:a.split(':')[1] for a in rec_tmp}
    tmp.update(para_sumar)
    result.append(tmp)
    
con_test = pd.DataFrame(result).sort_values(by = ['Persona','Grabacion'])
con_test.to_csv(os.path.join(ruta_gr,'packagename','training','data_testing.csv')
                ,quoting = csv.QUOTE_ALL,sep = ';',index = False)

# =============================================================================
# Datos entrenamiento
# =============================================================================
ruta_tr = os.path.join(ruta_gr,r'inputs/datos_entrenamiento')
#personas = glob(os.path.join(ruta,'ecg-id-database-1.0.0','P*','*'))
personas_tr = glob(os.path.join(ruta_tr,'p*','*'))
personas_tr = list(set([os.path.splitext(a)[0] for a in personas_tr]))

result = []
for per in personas_tr:
    tmp = dict()
    rec_tmp = wfdb.rdrecord(per).__dict__['comments']
    tmp['Persona'] = os.path.basename(os.path.dirname(per))
    tmp['Grabacion'] = os.path.basename(per)
    para_sumar = {a.split(':')[0]:a.split(':')[1] for a in rec_tmp}
    tmp.update(para_sumar)
    result.append(tmp)
    
con_train = pd.DataFrame(result).sort_values(by = ['Persona','Grabacion'])
con_train.to_csv(os.path.join(ruta_gr,'packagename','training','data_training.csv')
                ,quoting = csv.QUOTE_ALL,sep = ';',index = False)

# =============================================================================
# Lectura individual
# =============================================================================
# record_1 = wfdb.rdrecord(os.path.join(ruta,'ecg-id-database-1.0.0','Person_01','rec_1'))
# paciente = 'patient001'
# grabacion = 's0014lre'

# record_1 = wfdb.rdrecord(os.path.join(ruta_tr,paciente,grabacion))
# record_1.__dict__['p_signal'].shape
# wfdb.plot_wfdb(record_1, title=f' Record {grabacion} of {paciente} from Physionet')

# record = [wfdb.rdrecord(a).__dict__['p_signal'] for a in personas]
# record[0].T.shape
# ver = record_1.__dict__['sig_name']

# list(record_1.__dict__.keys())

