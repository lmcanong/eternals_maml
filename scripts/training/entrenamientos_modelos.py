# =============================================================================
# Paquetes
# =============================================================================
import os
import csv
import wfdb

import numpy as np
import pandas as pd

from time import time
from glob import glob
from sklearn.neural_network import MLPClassifier
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

import pywt
# =============================================================================
# Funciones
# =============================================================================
def transformar_wv(senal):
    y = senal
    x = range(len(y))
    ca, cd = pywt.dwt(y, 'db4')
    ya = pywt.idwt(ca, None, 'db4')
    yd = pywt.idwt(None, cd, 'db4')
    return yd.tolist()

ruta_gr = r'M:\Todo\Documentos\Cursos\Metodologías ágiles'
ruta_training = r'M:\Todo\Documentos\Cursos\Metodologías ágiles\eternals_maml\packagename\training/data_training.csv'
# =============================================================================
# Datos entrenamiento
# =============================================================================
ruta_tr = os.path.join(ruta_gr,r'eternals_maml_datos/ptb-diagnostic-ecg-database-1.0.0')
#personas = glob(os.path.join(ruta,'ecg-id-database-1.0.0','P*','*'))
personas_tr = glob(os.path.join(ruta_tr,'p*','*'))
personas_tr = list(set([os.path.splitext(a)[0] for a in personas_tr]))

scores = []
for a in range(15):
    t0 = time()
    senal = a
    records = [wfdb.rdrecord(a).__dict__['p_signal'][:,senal].tolist() for a in personas_tr]
    tamanos = [len(ele) for ele in records]
    records = [ele[-min(tamanos):] for ele in records]
    records_tr = [transformar_wv(ele) for ele in records]
    
    datos_training = pd.read_csv(ruta_training,sep=';')
    datos_training.columns.tolist()
    
    # ver = datos_training.groupby('Reason for admission',as_index=False)['Persona'].count()
    # ver.loc[ver['Persona']==1,'Reason for admission'].values.tolist()
    # X = np.asarray(records)
    X = np.asarray(records_tr)
    y = ['Heart failure' if ele in [' Heart failure (NYHA 2)',' Heart failure (NYHA 3)',' Heart failure (NYHA 4)'] else ele for ele in datos_training['Reason for admission'].tolist()]
    
    X_train, X_test, y_train, y_test = train_test_split(X, y,train_size = 0.70,random_state=1)
    
    clf = MLPClassifier(random_state=1, max_iter=300).fit(X_train, y_train)
    t1 = time()
    print((t1-t0)/60,'minutos')
    # clf.predict_proba(X_test[:1])
    y_pred = clf.predict(X_test)
    print(clf.score(X_test, y_test))
    print(classification_report(y_test,y_pred))
    scores.append([clf.score(X_test, y_test), classification_report(y_test,y_pred)])
    
