#paquetes
import os,re
import wget

#parámetros
nombre = 'inputs'

#creación de directorio para obtención de datos
if os.path.isdir(os.path.join('.',nombre)):
    print('El directorio ya existe')
else:
    os.mkdir(os.path.join('.',nombre))
    print(f'Se creo el directorio "{nombre}"')
    


#descarga datos de entrenamiento
url_datos_entrenamiento = 'https://www.physionet.org/static/published-projects/ptbdb/ptb-diagnostic-ecg-database-1.0.0.zip'
wget.download(url_datos_entrenamiento,out = os.path.join('.',nombre,'datos_entrenamiento.zip'))

#descarga datos de prueba
url_datos_prueba = 'https://physionet.org/static/published-projects/ecgiddb/ecg-id-database-1.0.0.zip'
wget.download(url_datos_prueba,out = os.path.join('.',nombre,'datos_prueba.zip'))
